package com.example.mvpdemo.`interface`

interface LogoutListener {
    fun logoutSuccess()
    fun logoutFail()
}