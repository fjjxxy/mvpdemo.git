package com.example.mvpdemo.`interface`

import com.example.mvpdemo.entity.WxOfficeBean

interface WxOfficeListListener {
    fun loadSuccess(data: List<WxOfficeBean>?)
    fun loadFail()
}