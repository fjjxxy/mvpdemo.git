package com.example.mvpdemo.`interface`

interface LoginListener {
    fun loginSuccess()
    fun loginFail(msg: String?)
}