package com.example.mvpdemo.view

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.mvpdemo.R
import com.example.mvpdemo.model.LogoutModel
import com.example.mvpdemo.presenter.ILogoutPresenter
import com.example.mvpdemo.presenter.LogoutPresenterImpl
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), LogoutView {
    private val logoutModel by lazy {
        LogoutModel()
    }
    private val logoutPresenterImpl: ILogoutPresenter by lazy {
        LogoutPresenterImpl(this, logoutModel)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initListener()
    }

    private fun initListener() {
        tv_get_wx_office.setOnClickListener {
            var intent = Intent(this, WxOfficeListActivity::class.java)
            startActivity(intent)
        }
        tv_logout.setOnClickListener {
            logoutPresenterImpl.logout()
        }
    }

    override fun logoutSuccess() {
        Toast.makeText(this, "退出成功", Toast.LENGTH_SHORT).show()
        var intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun logoutFail() {
        Toast.makeText(this, "退出失败", Toast.LENGTH_SHORT).show()
    }
}
