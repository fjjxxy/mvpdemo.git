package com.example.mvpdemo.view

interface LogoutView {
    fun logoutSuccess()
    fun logoutFail()
}