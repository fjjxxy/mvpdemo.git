package com.example.mvpdemo.view

import com.example.mvpdemo.entity.WxOfficeBean

interface WxOfficeListView {
    fun loadSuccess(data: List<WxOfficeBean>?)
    fun loadFail()
}