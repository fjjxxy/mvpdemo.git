package com.example.mvpdemo.view

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.mvpdemo.R
import com.example.mvpdemo.adapter.WxOfficeListAdapter
import com.example.mvpdemo.entity.WxOfficeBean
import com.example.mvpdemo.model.WxOfficeListModel
import com.example.mvpdemo.presenter.IWxOfficeListPresenter
import com.example.mvpdemo.presenter.WxOfficeListPresenter
import kotlinx.android.synthetic.main.activity_wx_office_list.*

class WxOfficeListActivity : AppCompatActivity(), WxOfficeListView {
    private val wxOfficeListModel by lazy {
        WxOfficeListModel()
    }
    private val wxOfficeListPresenter: IWxOfficeListPresenter by lazy {
        WxOfficeListPresenter(this, wxOfficeListModel)
    }
    private var list = arrayListOf<WxOfficeBean>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wx_office_list)
        rv_wx_office_list.layoutManager = LinearLayoutManager(this)
        rv_wx_office_list.adapter = WxOfficeListAdapter(this, list)
        refresh.setOnRefreshListener {
            loadData()
        }
        loadData()

    }

    private fun loadData() {
        list.clear()
        wxOfficeListPresenter.loadData()
    }

    override fun loadSuccess(data: List<WxOfficeBean>?) {
        refresh.isRefreshing = false
        Toast.makeText(this, "加载成功", Toast.LENGTH_SHORT).show()
        if (data != null) {
            list.addAll(data)
            rv_wx_office_list.adapter?.notifyDataSetChanged()
        }
    }

    override fun loadFail() {
        Toast.makeText(this, "加载失败", Toast.LENGTH_SHORT).show()
    }
}