package com.example.mvpdemo.view

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.mvpdemo.R
import com.example.mvpdemo.model.LoginModel
import com.example.mvpdemo.presenter.ILoginPresenter
import com.example.mvpdemo.presenter.LoginPresenterImpl
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity(), LoginView {
    private val loginModel: LoginModel by lazy {
        LoginModel()
    }
    private val loginPresenterImpl: ILoginPresenter by lazy {
        LoginPresenterImpl(this, loginModel)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initListener()
    }

    private fun initListener() {
        tv_login.setOnClickListener {
            loginPresenterImpl.login(et_account.text.toString(), et_password.text.toString())
        }
    }

    override fun showLoading() {
        Toast.makeText(this, "登录加载中", Toast.LENGTH_SHORT).show()
    }

    override fun hideLoading() {
        Toast.makeText(this, "登录加载结束", Toast.LENGTH_SHORT).show()
    }

    override fun loginSuccess() {
        Toast.makeText(this, "登录成功", Toast.LENGTH_SHORT).show()
        var intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun loginFail(msg: String?) {
        Toast.makeText(this, "登录失败", Toast.LENGTH_SHORT).show()
    }
}