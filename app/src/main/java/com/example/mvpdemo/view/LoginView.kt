package com.example.mvpdemo.view

/**
 * 用来View层更新UI的接口
 */
interface LoginView {
    /**
     * 显示加载中提示
     */
    fun showLoading()

    /**
     * 隐藏加载中提示
     */
    fun hideLoading()

    /**
     * 登陆成功的UI更新
     */
    fun loginSuccess()

    /**
     * 登录失败的UI更新
     */
    fun loginFail(msg: String?)

}