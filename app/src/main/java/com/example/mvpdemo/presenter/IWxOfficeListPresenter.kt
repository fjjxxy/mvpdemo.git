package com.example.mvpdemo.presenter

interface IWxOfficeListPresenter {
    fun loadData()
}