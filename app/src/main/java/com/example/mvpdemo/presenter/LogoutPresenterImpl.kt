package com.example.mvpdemo.presenter

import com.example.mvpdemo.`interface`.LogoutListener
import com.example.mvpdemo.model.ILogoutModel
import com.example.mvpdemo.view.LogoutView

class LogoutPresenterImpl(var logoutView: LogoutView, var iLogoutModel: ILogoutModel) :
    ILogoutPresenter,
    LogoutListener {

    override fun logout() {
        iLogoutModel.logout(this)
    }

    override fun logoutSuccess() {
        logoutView.logoutSuccess()
    }

    override fun logoutFail() {
        logoutView.logoutFail()
    }

}