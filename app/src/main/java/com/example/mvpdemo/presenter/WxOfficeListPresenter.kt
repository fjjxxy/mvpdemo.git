package com.example.mvpdemo.presenter

import com.example.mvpdemo.`interface`.WxOfficeListListener
import com.example.mvpdemo.entity.WxOfficeBean
import com.example.mvpdemo.model.IWxOfficeListModel
import com.example.mvpdemo.view.WxOfficeListView

class WxOfficeListPresenter(
    var wxOfficeListView: WxOfficeListView,
    var iWxOfficeListModel: IWxOfficeListModel
) : IWxOfficeListPresenter, WxOfficeListListener {

    override fun loadData() {
        iWxOfficeListModel.loadData(this)
    }

    override fun loadSuccess(data: List<WxOfficeBean>?) {
        wxOfficeListView.loadSuccess(data)
    }

    override fun loadFail() {
        wxOfficeListView.loadFail()
    }

}