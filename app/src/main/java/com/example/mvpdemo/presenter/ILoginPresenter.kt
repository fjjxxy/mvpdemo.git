package com.example.mvpdemo.presenter

/**
 * P层作为view与model的媒介，需要的接口方法
 */
interface ILoginPresenter {
    fun login(account: String, password: String)
}