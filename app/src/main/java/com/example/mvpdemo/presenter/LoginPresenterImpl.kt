package com.example.mvpdemo.presenter

import com.example.mvpdemo.`interface`.LoginListener
import com.example.mvpdemo.model.ILoginModel
import com.example.mvpdemo.view.LoginView

/**
 * 作为媒介联系view与model,view和model各司其职互不干扰，等待model返回回调，present再去通知view层进行UI更新
 */
class LoginPresenterImpl(loginView: LoginView, iLoginModel: ILoginModel) : ILoginPresenter,
    LoginListener {
    private var loginView: LoginView? = null
    private var loginModel: ILoginModel? = null

    init {
        this.loginModel = iLoginModel
        this.loginView = loginView
    }

    override fun login(account: String, password: String) {
        loginView?.showLoading()
        loginModel?.login(account, password, this)
    }

    override fun loginSuccess() {
        loginView?.hideLoading()
        loginView?.loginSuccess()
    }

    override fun loginFail(msg: String?) {
        loginView?.hideLoading()
        loginView?.loginFail(msg)
    }
}