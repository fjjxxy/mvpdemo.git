package com.example.mvpdemo.model

import com.example.mvpdemo.`interface`.LogoutListener

interface ILogoutModel {
    fun logout(logoutListener: LogoutListener)
}