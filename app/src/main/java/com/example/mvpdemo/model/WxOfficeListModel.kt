package com.example.mvpdemo.model

import com.example.kotlin_retrofit.http.Result
import com.example.kotlinandroid.http.HttpHelper
import com.example.kotlinandroid.http.SingleCallback
import com.example.mvpdemo.`interface`.WxOfficeListListener
import com.example.mvpdemo.entity.WxOfficeBean

class WxOfficeListModel : IWxOfficeListModel {
    override fun loadData(wxOfficeListListener: WxOfficeListListener) {
        HttpHelper.getApi()?.getWechatOffice()
            ?.enqueue(object : SingleCallback<Result<List<WxOfficeBean>>>() {
                override fun onSuccess(response: Result<List<WxOfficeBean>>) {
                    if ("0".equals(response.errorCode)) {
                        wxOfficeListListener.loadSuccess(response.data)
                    } else {
                        wxOfficeListListener.loadFail()
                    }
                }

            })
    }
}