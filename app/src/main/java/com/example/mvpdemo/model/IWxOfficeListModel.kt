package com.example.mvpdemo.model

import com.example.mvpdemo.`interface`.WxOfficeListListener

interface IWxOfficeListModel {
    fun loadData(wxOfficeListListener: WxOfficeListListener)
}