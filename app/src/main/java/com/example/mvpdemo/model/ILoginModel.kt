package com.example.mvpdemo.model

import com.example.mvpdemo.`interface`.LoginListener

/**
 * 处理数据的接口方法
 */
interface ILoginModel {
    fun login(account: String, password: String, loginListener: LoginListener)
}