package com.example.mvpdemo.model

import com.example.kotlin_retrofit.http.Result
import com.example.kotlinandroid.http.HttpHelper
import com.example.kotlinandroid.http.SingleCallback
import com.example.mvpdemo.`interface`.LogoutListener

class LogoutModel : ILogoutModel {
    override fun logout(logoutListener: LogoutListener) {
        HttpHelper.getApi()?.logout()?.enqueue(object : SingleCallback<Result<String>>() {
            override fun onSuccess(response: Result<String>) {
                if ("0".equals(response.errorCode)) {
                    logoutListener.logoutSuccess()
                } else {
                    logoutListener.logoutFail()
                }
            }

        })
    }

}