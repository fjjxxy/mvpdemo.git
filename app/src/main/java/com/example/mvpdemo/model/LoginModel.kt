package com.example.mvpdemo.model

import com.example.kotlin_retrofit.http.Result
import com.example.kotlinandroid.http.HttpHelper
import com.example.kotlinandroid.http.SingleCallback
import com.example.mvpdemo.`interface`.LoginListener
import com.example.mvpdemo.entity.UserBean

class LoginModel : ILoginModel {
    override fun login(account: String, password: String, loginListener: LoginListener) {
        HttpHelper.getApi()?.login(account, password)
            ?.enqueue(object : SingleCallback<Result<UserBean>>() {
                override fun onSuccess(response: Result<UserBean>) {
                    if ("0".equals(response.errorCode)) {
                        loginListener.loginSuccess()
                    } else {
                        loginListener.loginFail(response.errorMsg)
                    }
                }


            })
    }
}