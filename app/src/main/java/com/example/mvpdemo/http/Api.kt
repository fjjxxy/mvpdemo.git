package com.example.kotlin_retrofit.http

import com.example.mvpdemo.entity.UserBean
import com.example.mvpdemo.entity.WxOfficeBean
import retrofit2.Call
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST

interface Api {
    /**
     * 登录
     */
    @POST("/user/login")
    @FormUrlEncoded
    fun login(@Field("username") account: String, @Field("password") password: String): Call<Result<UserBean>>

    /**
     * 退出
     */
    @GET("/user/logout/json")
    fun logout(): Call<Result<String>>

    /**
     * 获取公众号列表
     */
    @GET("/wxarticle/chapters/json")
    fun getWechatOffice(): Call<Result<List<WxOfficeBean>>>

}