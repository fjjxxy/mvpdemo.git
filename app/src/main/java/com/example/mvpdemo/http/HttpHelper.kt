package com.example.kotlinandroid.http

import com.example.kotlin_retrofit.http.Api
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit

/**
 * 接口请求工具类
 */
class HttpHelper {
    companion object {
        private val BASE_URL = "https://www.wanandroid.com"
        private var sHttpClient: OkHttpClient? = null
        private var sApi: Api? = null

        fun getHttpClient(): OkHttpClient? {
            if (sHttpClient == null) {
                synchronized(HttpHelper::class.java) {
                    if (sHttpClient == null) {
                        val builder = OkHttpClient.Builder()
                        /**
                         * 打印日志
                         */
                        var interceptor = HttpLoggingInterceptor()
                        interceptor.level = HttpLoggingInterceptor.Level.BODY
                        builder.addInterceptor(interceptor)
                        sHttpClient = builder
                            .callTimeout(40, TimeUnit.SECONDS)
                            .connectTimeout(40, TimeUnit.SECONDS)
                            .readTimeout(40, TimeUnit.SECONDS)
                            .writeTimeout(40, TimeUnit.SECONDS)
                            .build()
                    }
                }
            }
            return sHttpClient
        }

        fun getApi(): Api? {
            if (sApi == null) {
                synchronized(HttpHelper::class.java) {
                    if (sApi == null) {
                        val retrofit = createRetrofit(BASE_URL)
                        sApi = retrofit.create(Api::class.java)
                    }
                }
            }
            return sApi
        }

        private fun createRetrofit(baseUrl: String): Retrofit {
            val gsonBuilder = GsonBuilder()
            return Retrofit.Builder()
                .client(getHttpClient())
                .baseUrl(baseUrl)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                .build()
        }
    }
}