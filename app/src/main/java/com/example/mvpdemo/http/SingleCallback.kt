package com.example.kotlinandroid.http

import android.util.Log
import android.widget.Toast
import com.example.kotlin_retrofit.http.Result
import com.example.mvpdemo.App
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.internal.EverythingIsNonNull
import java.net.UnknownHostException

/**
 * 接口请求基本回调
 */
abstract class SingleCallback<T : Result<*>?> :
    Callback<T> {
    override fun onResponse(
        call: Call<T>,
        response: Response<T>
    ) {
        val result = response.body()
        if (result == null) {
            onFailure("-99", "接口请求失败", null)
        } else {
            if (apiFailure(result)) {
                onFailure(result.errorCode, result.errorMsg ?: "", result)
            } else {
                onSuccess(result)
            }
        }
    }

    @EverythingIsNonNull
    override fun onFailure(call: Call<T>, t: Throwable) {
        Log.e("SingleCallback", "onFailure: " + t.message, t)
        if (t is UnknownHostException) {
            onFailure("-99", "接口请求失败", null)
        } else if (!"socket closed".equals(t.localizedMessage, ignoreCase = true)
            && !"canceled".equals(t.localizedMessage, ignoreCase = true)
        ) {
            onFailure("-999", "接口请求失败", null)
        }
    }

    fun successCode(): String {
        return "0"
    }

    fun apiFailure(result: T?): Boolean {
        return result == null || successCode() != result.errorCode
    }

    /**
     * 失败回调。默认已经将失败信息提示出来
     */
    fun onFailure(code: String?, message: String, response: T?) {
        Toast.makeText(App.getContext(), message, Toast.LENGTH_SHORT).show()
    }

    abstract fun onSuccess(response: T)

}