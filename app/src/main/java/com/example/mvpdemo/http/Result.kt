package com.example.kotlin_retrofit.http

class Result<T> {
    var errorMsg: String? = null
    var errorCode: String? = null
    var data: T? = null
}
